import React from "react";

const StartStop = ({ func, icon, color }) => {
  return (
    <div style={{ margin: "5px" }}>
      <button
        className={`btn btn-outline-${color}`}
        style={{ width: "150px" }}
        onClick={func}
      >
        {icon}
      </button>
    </div>
  );
};

export default StartStop;

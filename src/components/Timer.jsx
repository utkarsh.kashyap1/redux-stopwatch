import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FaPlayCircle, FaPauseCircle, FaRegPlayCircle, FaUndoAlt, FaSave } from "react-icons/fa";

import {
  lap_timer,
  pause_timer,
  reset_timer,
  resume_timer,
  start_timer,
} from "../actions/timerActions";
import Reset from "./Reset";
import StartStop from "./StartStop";
import Lap from "./Lap";

const Timer = () => {
  const dispatch = useDispatch();
  const [timer, setTimer] = useState(0);
  const { isActive, isPaused } = useSelector((state) => state.timer);
  const increment = useRef(null);

  const handleStart = () => {
    dispatch(start_timer());
    increment.current = setInterval(() => {
      setTimer((timer) => timer + 1);
    }, 1000);
  };

  const handlePause = () => {
    clearInterval(increment.current);
    dispatch(pause_timer());
  };

  const handleResume = () => {
    dispatch(resume_timer());
    increment.current = setInterval(() => {
      setTimer((timer) => timer + 1);
    }, 1000);
  };

  const handleReset = () => {
    clearInterval(increment.current);
    dispatch(reset_timer());
    setTimer(0);
  };

  const formatTime = (timer) => {
    const getSeconds = `0${timer % 60}`.slice(-2);
    const minutes = `${Math.floor(timer / 60)}`;
    const getMinutes = `0${minutes % 60}`.slice(-2);
    const getHours = `0${Math.floor(timer / 3600)}`.slice(-2);

    return `${getHours} : ${getMinutes} : ${getSeconds}`;
  };

  const handleLap = () => {
    dispatch(lap_timer(formatTime(timer)));
  };

  return (
    <div>
      <h1 className="card-title">{formatTime(timer)}</h1>
      <div className="buttons" style={{width:'40%', margin:'auto'}}>
        {!isActive && !isPaused ? (
          <StartStop func={handleStart} icon={<FaPlayCircle fontSize={'27px'} />} color="success" />
        ) : isPaused ? (
          <StartStop func={handlePause} icon={<FaPauseCircle fontSize={'27px'} />} color="warning" />
        ) : (
          <StartStop func={handleResume} icon={<FaRegPlayCircle fontSize={'27px'} />} color="primary" />
        )}
        <Reset func={handleReset} isDisabled={!isActive} icon={<FaUndoAlt fontSize={'27px'} />} />
        <Lap handleLap={handleLap} icon={<FaSave fontSize={'27px'} />} />
      </div>
    </div>
  );
};

export default Timer;

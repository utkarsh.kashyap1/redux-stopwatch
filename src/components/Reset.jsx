import React from "react";

const Reset = ({ func, icon, isDisabled }) => {
  return (
    <div style={{margin:'5px'}}>
      <button
        className="btn btn-outline-danger"
        disabled={isDisabled}
        onClick={func}
        style={{width:'150px'}}
      >
        {icon}
      </button>
    </div>
  );
};

export default Reset;

import React from "react";

const Lap = ({ handleLap, icon }) => {
  return (
    <div style={{ margin: "5px" }}>
      <button
        className="btn btn-outline-secondary"
        style={{ width: "150px" }}
        onClick={handleLap}
      >
        {icon}
      </button>
    </div>
  );
};

export default Lap;

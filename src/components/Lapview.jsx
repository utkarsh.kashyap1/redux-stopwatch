import React from 'react'
import { useSelector } from 'react-redux'

const Lapview = () => {
  const laps = useSelector(state => state.timer.laps)
  return (
    <div className='mt-3'>
      {laps && laps.length > 0 ? 
        laps.map(lap => (
          <div>
            <h4>{lap}</h4>
          </div>
        ))
      :
        <h4>No Laps!</h4>
      }
    </div>
  )
}

export default Lapview
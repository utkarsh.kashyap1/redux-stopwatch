import { combineReducers } from "redux";
import { timerReducers } from "./timerReducers";

export const rootReducer = combineReducers({
    timer: timerReducers,
});
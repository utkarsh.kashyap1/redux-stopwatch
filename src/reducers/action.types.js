export const START_TIMER = 'START_TIMER';
export const RESET_TIMER = 'RESET_TIMER';
export const CREATE_LAP = 'CREATE_LAP';
export const PAUSE_TIMER = 'PAUSE_TIMER';
export const RESUME_TIMER = 'RESUME_TIMER';
import {
  CREATE_LAP,
  RESET_TIMER,
  START_TIMER,
  PAUSE_TIMER,
  RESUME_TIMER,
} from "./action.types";

const initialState = {
  isPaused: false,
  isActive: false,
  laps: [],
};

export const timerReducers = (state = initialState, action) => {
  switch (action.type) {
    case START_TIMER:
      return { ...state, isActive: true, isPaused: true };
    case PAUSE_TIMER:
      return { ...state, isPaused: false };
    case RESUME_TIMER:
      return { ...state, isPaused: true };
    case RESET_TIMER:
      return initialState;
    case CREATE_LAP:
      return { ...state, laps: state.laps.concat([action.payload]) };
    default:
      return state;
  }
};

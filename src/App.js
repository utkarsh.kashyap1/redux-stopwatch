import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Timer from "./components/Timer";
import Lapview from "./components/Lapview";
import "./App.css";

const App = () => {
  return (
    <div className="container mt-3" style={{textAlign: 'center'}}>
      <div className="card">
        <div className="card-body">
          <Timer />
          <Lapview />
        </div>
      </div>
    </div>
  );
};

export default App;

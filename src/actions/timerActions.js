import {
  CREATE_LAP,
  RESET_TIMER,
  START_TIMER,
  PAUSE_TIMER,
  RESUME_TIMER,
} from "../reducers/action.types";

export const start_timer = () => {
  return { type: START_TIMER };
};

export const lap_timer = (lap) => {
  return { type: CREATE_LAP, payload: lap };
};

export const reset_timer = () => {
  return { type: RESET_TIMER };
};

export const pause_timer = () => {
  return { type: PAUSE_TIMER };
};

export const resume_timer = () => {
  return { type: RESUME_TIMER };
};

